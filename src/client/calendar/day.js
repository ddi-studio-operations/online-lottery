import React from 'react';
import View from '../framework/view';
import Checkbox from '../inputs/checkbox';

export default class Day extends View.Cascading {
  update(prize, selected) {
    prize.selected = selected;
    console.error('NYI');
  }
  render() {
    console.log(this.model.prizes);
    return (
      <td className={this.model.enabled ? 'enabled' : 'disabled'}>
        <header>{this.model.date.observableSource.getDate()}</header>
        <ul>
          {this.model.prizes.map(prize => {
            return (
              <li key={'prize-' + prize.id}>
                <Checkbox changed={value => this.update(prize, value)}
                          checked={prize.selected}
                          label={prize.name}
                />
              </li>
            );
          })}
        </ul>
      </td>
    );
  }
}
