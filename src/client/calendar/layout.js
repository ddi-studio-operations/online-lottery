import React from 'react';
import View from '../framework/view';
import Day from './day';

export default class Calendar extends View {
  render() {
    return (
      <table className="calendar">
        <thead>
          <tr>
            {this.model.days.map(day => {
              return (<th key={day}>{day}</th>);
            })}
          </tr>
        </thead>
        <tbody>
          {this.model.weeks.map((week, w) => {
            return (<tr key={'week' + w.toString()}>
              {week.days.map((day, d) => {
                return (
                  <Day key={'day' + d.toString()} model={day} />
                );
              })}
            </tr>);
          })}
        </tbody>
      </table>
    );
  }
}
