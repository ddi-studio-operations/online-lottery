import Compat from '../services/compat';
Compat.test(() => typeof(Proxy) !== 'undefined');

export default class Observable {
  constructor(source) {
    var cache = {}, listeners = [], wrappers;
    var subscribe = (listener) => { if (typeof(listener) === 'function') { listeners.push(listener); } };
    var unsubscribe = (listener) => { listeners = listeners.filter(l => l !== listener); };
    var notify = (data) => listeners.forEach((listener) => listener(data));
    var revoke = () => {
      notify = null;
      Object.values(cache).forEach(c => c.revoke());
      cache = null;
      listeners.length = 0;
      listeners = null;
      wrapper.revoke();
    }

    var wrapper = Proxy.revocable(source, {
      get: (s, m) => {
        if (m === 'observableSource') { return source; }
        if (m === 'isObservable') { return true; }
        if (m === 'subscribe') { return subscribe; }
        if (m === 'unsubscribe') { return unsubscribe; }
        if (m === 'revoke') { return revoke; }
        if (cache[m]) { return cache[m]; }
        if (typeof(source[m]) !== 'object') { return source[m]; }
        cache[m] = new Observable(source[m]);
        cache[m].subscribe(notify);
        return cache[m];
      },
      set: (s, m, v) => {
        source[m] = v;
        delete cache[m];
        notify({
          source: s,
          member: m
        });
        return true;
      }
    });

    return wrapper.proxy;
  }
}
