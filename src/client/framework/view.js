import React from 'react';
import Observer from './observable';

export default class View extends React.Component {
  constructor(props) {
    super(props);
    if (typeof(props['model']) === 'undefined') { console.warn('View without model'); }
    else {
      this.model = props['model'].isObservable ? props['model'] : new Observer(props['model']);
      this.model.subscribe((data) => { if (data.source === this.model) { this.forceUpdate(); } });
    }
  }

  render() {
    return (<span>{View.getClassName(this.model)}</span>);
  }

  static getClassName(obj) {
    if (typeof(obj) === 'undefined') { return 'undefined'; }
    if (!model.constructor || !model.constructor.name) { return Object.prototype.toString.call(obj); }
    return '[class ' + model.constructor.name + ']';
  }

  componentWillUnmount() {
    if (this.model) { this.model.revoke(); }
  }
}

View.Cascading = class extends View {
  constructor(props) {
    super(props);
    if (this.model) {
      this.model.subscribe((data) => { if (data.source !== this.model) { this.forceUpdate(); } });
    }
  }
}
