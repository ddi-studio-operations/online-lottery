import React from 'react';
import ReactDOM from 'react-dom';
import Calendar from './calendar/layout';

var thing = {
  days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
  weeks: [
    {
      days: [-2, -1, 0, 1, 2, 3, 4].map(offset => {
        var date = new Date();
        date.setDate(date.getDate() + offset);
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        return {
          date: date,
          prizes: [1, 2, 3].map(index => {
            return {
              id: (offset + 100) * 10 + index,
              name: 'prize ' + index,
              selected: index == 2
            };
          })
        };
      })
    }
  ]
}

ReactDOM.render(
  (<Calendar model={thing} />),
  document.querySelector('main')
);
