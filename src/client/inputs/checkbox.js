import React from 'react';

export default class Checkbox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      label: (props['label'] || ''),
      changed: (props['changed'] || (() => {})),
      checked: (props['checked'] === true ? true : false)
    }
  }
  render() {
    return (
      <label>
        <input type="checkbox"
               checked={this.state.checked}
               onChange={() => {
                 this.setState({checked: !this.state.checked});
                 this.state.changed(!this.state.checked);
               }}
        />
        <span>{this.label}</span>
      </label>
    );
  }
}
