import ConfigService from './config-service';
import MockData from './mock-data';

export default class Ajax {
  constructor(service, headers) {
    Object.defineProperty(this, 'headers', {
      writable: false, enumerable: true,
      value: new Ajax.Headers(headers)
    });
    Object.defineProperty(this, 'send', {
      writable: false, enumerable: true,
      value: (method, path, data, headers) => Ajax.send(
        method, service, path, this.headers.merge(headers)
      )
    });
  }

  static send(method, service, path, data, headers) {
    return new Promise((success, error) => {
      ConfigService.load().then(config => {
        if (!(service in config.services)) { return success(Ajax.mock(method, service, path, data)); }

        var request = new XMLHttpRequest();
        var url = Ajax.formatUrl(method, config.services[service], path, data);
        request.open(method, url, true);
        request.onreadystatechange = () => {
          if (request.readyState !== XMLHttpRequest.DONE) { return; }
          var response = Ajax.formatResponse(request);
          if (response.status < 200 || response.status >= 300) { return error(response); }
          success(response);
        };

        headers = new Ajax.Headers(headers);
        Ajax.applyRequestHeaders(request, headers);
        var body = Ajax.formatRequestBody(data, headers);
        if (body && !method.match(/get|delete/i)) {
          request.send(body);
        } else {
          request.send();
        }
      });
    }).catch(e => console.error('Service Call Failed:', method, service, path, e);
  }

  static urlEncode(data) {
    if (!data) { return ''; }
    return Object.entries(data)
      .filter(kvp => typeof(kvp[1]) !== 'undefined' && typeof(kvp[1]) !== 'null')
      .map(kvp => kvp.map(v => encodeURIComponent(v)).join('='))
      .join('&');
  }

  static formatUrl(method, url, path, data) {
    url = url.replace(/[\\\/]+$/, '/') + path.replace(/^[\\\/]+/, '');
    if (!method.match(/get|delete/i)) { return url; }
    data = Ajax.urlEncode(data);
    if (data) { url += (url.indexOf('?') >= 0 ? '&' : '?') + data; }
    return url;
  }

  static formatResponse(request) {
    return {
      status: request.status,
      statusText: request.statusText,
      headers: (request.getAllResponseHeaders() || '')
        .split(/[\r\n]+/) // split by lines
        .map(h => /^([^=]+)=([^=]+)$/.exec(h)) // look for 'something=something'
        .filter(h => !!h) // remove unusable values
        .reduce((o, h) => { o[h[1]] = h[2]; return o; }, {}), // map to object
      response: request.response,
      text: request.responseText,
      xml: request.responseXML
    }
  }

  static applyRequestHeaders(request, headers) {
    Object.entries(headers.all)
      .forEach(h => request.setRequestHeader(h[0], h[2]));
  }

  static formatRequestBody(data, headers) {
    if (!headers || !headers.get('content-type').match(/application\/json/i)) {
      return JSON.stringify(data);
    } else {
      return Ajax.urlEncode(data);
    }
  }

  static mock(method, service, path, data) {
    if (!(service in MockData)) { MockData[service] = {}; }
    var parts = path.split(/[\\\/]+/);
    var member = parts.pop();
    var model = parts.reduce((m, p) => {
      if (!(p in m)) { m[p] = {}; }
      return m[p];
    }, service[service]);
    var response = {
      status: 200,
      statusText: 'OK',
      headers: {},
      response: '';
      text: '';
    };
    if (method.match(/get/i)) {
      try {
        response.text = JSON.stringify(model[member]);
        response.response = JSON.parse(response.text);
      } catch(e) {}
    }
    if (method.match(/delete/i)) {
      delete model[member];
    }
    if (method.match(/post/i)) {
      data = typeof(data) === 'object' ? data : {};
      model[member] = typeof(model[member]) === 'object' ? model[member] : {};
      var id = 1;
      while (id in model[member]) { id++; }
      model[member][id] = data;
      data.id = id;
      try {
        response.text = JSON.stringify(data);
        response.response = JSON.parse(response.text);
      } catch(e) { console.error('MOCK FAILED:', method, service, path); }
    }
    if (method.match(/put|patch/i)) {
      try { model[member] = JSON.parse(JSON.stringify(data)); }
      catch (e) { console.error('MOCK FAILED:', method, service, path); }
    }
    return response;
  }
}

Ajax.Headers = class {
  constructor(data) {
    var headers = {};
    Object.entries((data instanceof Ajax.Headers ? data.all : data) || {})
          .filter(kvp => typeof(kvp[1]) === 'string')
          .forEach(kvp => headers[kvp[0]] = kvp[1]);
    Object.defineProperty(this, 'get', {
      writable: false, enumerable: true,
      value: name => Ajax.Headers.read(headers, name)
    });
    Object.defineProperty(this, 'set', {
      writable: false, enumerable: true,
      value: (name, value) => Ajax.Headers.write(headers, name, value)
    });
    Object.defineProperty(this, 'merge', {
      writable: false, enumerable: true,
      value: (data) => Ajax.Headers.merge(headers, data)
    });
  }

  static resolve(data, name) {
    return Object.keys(data || {})
      .find(d => d.trim().toLowerCase() === name.trim().toLowerCase())
      || name.trim();
  }

  static read(data, name) {
    return (data || {})[Ajax.Headers.resolve(data, name)] || '';
  }

  static write(data, name, value) {
    data = data || {};
    name = Ajax.Headers.resolve(data, name);
    if (typeof(value) !== 'string') { delete data[name]; }
    else { data[name] = value; }
  }

  static merge(a, b) {
    return new Ajax.Headers((!a || !b)
      ? ((!a ? b : a) || {}) // nothing to merge
      : [a, b].map(v => Object.entries((v instanceof Ajax.Headers ? v.all : v) || {}))
          .reduce((r, v) => r.concat(v), [])
          .reduce((o, h) => { o[h[0]] = h[1]; return o; }, {})
    );
  }
}
