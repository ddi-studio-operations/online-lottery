var state = false;
var listeners = [];

export default class Compat {
  static test(test) { try { if (!test()) { Compat.notify(); } } catch(e) { Compat.notify(); } }
  static notify() { state = true; listeners.forEach(l => l()); }
  static subscribe(listener) { listeners.push(listener); if (state) { listener(); } }
  static unsubscribe(listener) { listeners = listeners.filter(l => l !== listener); }
}
