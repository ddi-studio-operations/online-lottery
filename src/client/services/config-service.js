export default class ConfigService {
    static load() {
        return new Promise((success, error) => {
          var request = new XMLHttpRequest();
          request.open('GET', 'config.json', true);
          request.onreadystatechange = () => {
            if (request.readyState !== XMLHttpRequest.DONE) { return; }
            if (request.status < 200 || request.status >= 300) {
              return error('status: ' + request.status.toString());
            }
            try { success(JSON.parse(request.responseText)); }
            catch(e) { error(e); }
          };
          request.send();
        }).catch(e => console.error('Config failed to load:', e));
    }
}
