var posix = require('path').posix;
var config = require('./config');
var gulp = require('gulp');
var render = require('string-template');
var minify = require('html-minifier').minify;
var System = require('./services/System');
var Compile = require('./services/Compile');

gulp.task('build-client', complete => {
  // Clear existing build...
  System.clear(config.clientTarget)
    .catch(e => console.error(e))
    .then(() => {

      // Find all templates in ./src/client
      System.search(config.clientSource, /\.template$/gi)
        .catch(e => { console.error(e); throw 'build-client halted'; })
        .then(templates => {
          new Promise((success, error) => {
            templates
              .map(t => System.parsePath(t))
              .forEach(template => {

                console.log('Resolving template: ', template.name);

                // Find each name-matched sibling file
                var siblings = System.filesSync(template.directory, template.name + '\\.[^\\.]+$')
                  .map(s => System.parsePath(s))
                  .filter(s => s.extension.toLowerCase() !== 'template');

                console.log('Siblings Detected: ', siblings.map(s => s.file).join(','));

                var context = {};
                // Process each file and store to context[extension name]
                Promise.all(siblings
                  .map(s => Compile.detect(s.path)
                    .then(content => context[s.extension] = content)
                  )
                ).catch(e => error(e))
                .then(() => {
                  // Compile and minify the template
                  var tmplContent = System.read(template.path);
                  var rendered = render(tmplContent, context);
                  if (config.minified) {
                    rendered = minify(rendered, {
                      caseSensitive: true,
                      keepClosingSlash: true,
                      minifyCSS: true,
                      minifyJS: true,
                      removeComments: true,
                      collapseWhitespace: true
                    });
                  }

                  // Write template to output
                  System.write(
                    posix.resolve(config.clientTarget, template.name + '.html'),
                    rendered
                  );
                  success();
                });
              });
          }).catch(e => console.error(e))
          .then(() => complete());
        });
    });
});
