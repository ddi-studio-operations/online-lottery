var System = require('./System');

class Compile {
  static detect(path) {
    var info = System.parsePath(path);
    if (info.extension.toLowerCase() === 'js') { return Compile.js(path); }
    if (info.extension.toLowerCase() === 'less') { return Compile.less(path); }
    if (info.extension.toLowerCase() === 'json') { return Compile.json(path); }
    return new Promise((success, error) => success(System.read(path)));
  }

  static js(path) {
    return new Promise((success, error) => {
      console.log('Compiling JS: ', path);
      require('browserify')(path)
        .transform("babelify", {presets: ["es2015", "react"]})
        .bundle((e, s) => {
          if (e) { return error(e); }
          success(s.toString());
        });
    });
  }

  static less(path) {
    var info = System.parsePath(path);
    return new Promise((success, error) => {
      console.log('Compiling LESS: ', path);

      require('less')
        .render("@import '" + path + "';",
          { paths: [path.directory] },
          (e, s) => {
            if (e) { return error(e); }
            return success(s.css);
          });
    });
  }

  static json(path) {
    return new Promise((success, error) => {
      console.log('Compiling JSON: ', path);
      success(require(path));
    });
  }
}

module.exports = Compile;
