var fs = require('fs');
var posix = require('path').posix;

class System {
  static files(path, filter) {
    return new Promise((success, filter) => {
      try { success(System.filesSync(path, filter)); }
      catch(e) { error(e); }
    });
  }

  static filesSync(path, filter) {
    var files = fs.readdirSync(path)
      .map(p => posix.resolve(path, p))
      .filter(p => fs.lstatSync(p).isFile());
    if (!filter) { return files; }
    return files.filter(f => f.match(filter));
  }

  static search(path, filter) {
    return new Promise((success, error) => {
      try { success(System.searchSync(path, filter)); }
      catch(e) { error(e); }
    });
  }

  static searchSync(path, filter) {
    var files = [];
    var contents = fs
      .readdirSync(path)
      .map(p => posix.resolve(path, p))
      .map(p => { return { path: p, isDir: fs.lstatSync(p).isDirectory() }; });
    contents.forEach(v => v.isDir
      ? files = files.concat(System.searchSync(v.path, filter))
      : files.push(v.path)
    );
    if (!filter) { return files; }
    return files.filter(f => f.match(filter));
  }

  static clear(path) {
    return new Promise((success, error) => {
      try { System.clearSync(path); success(); }
      catch(e) { error(e); }
    });
  }

  static clearSync(path) {
    if (!fs.existsSync(path)) { return; }
    fs.readdirSync(path).forEach(file => {
      var target = posix.resolve(path, file);
      if (fs.lstatSync(target).isDirectory()) {
        System.clearSync(target);
      } else {
        fs.unlinkSync(target);
      }
    });
    fs.rmdirSync(path);
  }

  static parsePath(path) {
    var directory = posix.dirname(path);
    var file = posix.basename(path);
    var name = file.replace(/\.[^\.]+$/gi, '');
    var extension = (/\.([^\.]+)$/gi.exec(file) || [])[1];
    return {
      path: path,
      directory: directory,
      file: file,
      name: name,
      extension: extension
    }
  }

  static read(path) {
    return fs.readFileSync(path, 'utf8');
  }

  static write(path, content) {
    System.createPath(posix.dirname(path));
    fs.writeFileSync(path, content);
  }

  static createPath(path) {
    path.split(posix.sep)
        .reduce((parent, child) => {
          var target = posix.resolve(parent, child);
          if (!fs.existsSync(target)) { fs.mkdirSync(target); }
          return target;
        }, posix.isAbsolute(path) ? posix.sep : '');
  }
}

module.exports = System;
